%%% LaTeX document class for typesetting minutes of meetings
%%% Leeds Minster
%%%
%%% Created by: Simon Peatman (PCC Secretary, St Luke's, Reading)
%%% Version 1.0 (24 June 2017)
%%% Version 1.1 (27 September 2017) - meeting type no longer hard-coded
%%%                                 - added ``also present'' section
%%% Version 2.0 (24 March 2018)     - changed from Luke and Bart to Redlands Parish Church
%%% Version 3.0 (13 July 2022)      - changed from Redlands Parish Church to Leeds Minster
%%%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{minster}[2022/07/13 Minutes of meetings at Leeds Minster]

% Base this documentclass on {article}
\LoadClass[11pt]{article}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

% Packages
\RequirePackage{geometry}
\RequirePackage{graphicx}
\RequirePackage{xcolor}
\RequirePackage{tabularx}
\RequirePackage{ifthen}

% Page layout
\geometry{a4paper, total={180mm,274mm}, left=15mm, top=9mm}
\RequirePackage[none]{hyphenat}
\pagestyle{empty}

% Font
%\renewcommand{\familydefault}{\sfdefault}

% Title and sign-off
\newcommand{\meeting}[1]{\gdef\@meeting{#1}}
\newcommand{\@meeting}{???MEETING???}
\newcommand{\venue}[1]{\gdef\@venue{#1}}
\newcommand{\@venue}{???VENUE???}
\newcommand{\present}[1]{\gdef\@present{#1}}
\newcommand{\@present}{???PRESENT???}
\newcommand{\alsopresent}[1]{\gdef\@alsopresent{#1}}
\newcommand{\@alsopresent}{}
\renewcommand{\@date}{???DATE???}
\renewcommand{\@author}{???AUTHOR???}
\newcommand{\position}[1]{\gdef\@position{#1}}
\newcommand{\@position}{???POSITION???}
\renewcommand\maketitle{\begin{center}\includegraphics[width=10cm]{letterhead}\medskip

\Large{\bf{\@meeting\ minutes, \@date\ifthenelse{\equal{\@venue}{}}{}{, \@venue}}}\end{center}\normalsize
\ifthenelse{\equal{\@present}{}}{}{
\setlength{\tabcolsep}{0.5em}
\renewcommand{\arraystretch}{1.5}
\newlength{\firstcolwidth}
\settowidth{\firstcolwidth}{\ifthenelse{\equal{\@alsopresent}{}}{\bf{Present:}}{\it{Also present:}}}
\begin{tabularx}{.995\textwidth}{p{\firstcolwidth}X}
 \bf{Present:} & \@present\ifthenelse{\equal{\@alsopresent}{}}{}{\\\it{Also present:} & \it{\@alsopresent}}
\end{tabularx}
}
\setlength{\tabcolsep}{6pt}
\renewcommand{\arraystretch}{1.15}
}
\newcommand\signoff{\begin{flushright}\@author\\\@position\end{flushright}

Signed: \makebox[85mm]{\dotfill}\hspace{5mm}Date: \makebox[60mm]{\dotfill}}

% Sections and subsections
\renewcommand\thesection{\arabic{section}.}
\renewcommand\thesubsection{\arabic{section}.\arabic{subsection}.}
%\def\removes@@dot\csname the#1\endcsname{\arabic{#1}}
%\def\p@section{\removes@@dot}
\renewcommand{\section}{\leftskip 0mm\@startsection{section}{0}{0mm}{4mm}{0.01mm}{\bf}}
\newcommand\Xsubsection{\leftskip 0mm\@startsection{subsection}{1}{5mm}{4mm}{0.01mm}{\bf}}
\renewcommand{\subsection}[1]{\Xsubsection{#1}\leftskip 5mm}

% Paragraphs
\setlength{\parindent}{0mm}
\setlength{\parskip}{2mm}

% Motions voted upon, decisions and action points
\newcommand\motion[4]{\setlength{\tabcolsep}{0.5em}
{\color{blue}\begin{center}\begin{tabularx}{.7\textwidth}{|p{4em}X|}
\hline
\textbf{Motion:} & #1\\
\hline
\multicolumn{2}{|c|}{\textit{Proposed:} #2 \hfill \textit{Seconded:} #3 \hfill \textbf{#4}}\\
\hline
\end{tabularx}\end{center}}
\setlength{\tabcolsep}{6pt}
}
\newcommand\chairmotion[3]{\setlength{\tabcolsep}{0.5em}
{\color{blue}\begin{center}\begin{tabularx}{.7\textwidth}{|p{4em}X|}
\hline
\textbf{Motion:} & #1\\
\hline
\multicolumn{2}{|c|}{\textit{Proposed:} #2 \hfill \textbf{#3}}\\
\hline
\end{tabularx}\end{center}}
\setlength{\tabcolsep}{6pt}
}
\newcommand\decision[1]{{\bf\color{blue}{#1}}}
\newcommand\action[1]{\hspace*{\fill}{\bf\color{red}{Action:~#1}}}
